//
//  AppDelegate.h
//  BaseDemo
//
//  Created by 刘剑锋 on 2020/7/13.
//  Copyright © 2020 Liujianfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

