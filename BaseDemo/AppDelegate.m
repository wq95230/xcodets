//
//  AppDelegate.m
//  BaseDemo
//
//  Created by 刘剑锋 on 2020/7/13.
//  Copyright © 2020 Liujianfeng. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
       self.window.rootViewController = [ViewController new];
       
       self.window.backgroundColor = [UIColor  whiteColor];
    
    
    return YES;
}



@end
